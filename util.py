import numpy as np
import scipy.special as sps

class Util:

	def skewSymmetric(self, vector):
		temp = np.array(((0, -1*vector[2], vector[1]),(vector[2],0,-1*vector[0]),(-1*vector[1], vector[0], 0)), float)
		return temp

	"""
	Is a function of dimension. Change when dimension is changed
	"""
	def xi(self, rho):
#		denom = (2*np.pi)
#		num = np.exp((-1*(rho**2))/2.0)
		denom = (2*np.pi)
		num = np.exp((-1*(rho**2))/2)
		return num/denom

	def G(self, rho, sigma):
		denom = (np.pi*sigma**2)/2.0
		num = np.exp((-1*(rho**2))/2)
		return num/denom



	def green(self, rho):
		denom = 4*np.pi*rho
		num = sps.erf(rho/np.sqrt(2)) 
#		if rho != 0:
		return num/denom
#		else :
#			return 0


	"""
	2D Kernel from Barba 04
	"""
	def K(self, rho):
		num = (1-np.exp(-1*(rho**2)/2)) 
		denom = 2*np.pi*rho**2
		return num/denom

	def F(self, rho):
#		if rho != 0:
			return (3*self.K(rho)-self.xi(rho))/(rho**2)
#		else:
#			return 0




