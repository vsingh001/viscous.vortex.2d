from init import Init
from integrate import Integrate
#from convect import Integrate

class VisVortex:

	def runVisVort(self):
		initObj = Init()
		x, y, alphaZ, h, sigma, nu = initObj.init()

		integObj = Integrate()
		#At a later stage this can be file input and passed
		integObj.constants()
		integObj.pushInitValues(x, y, alphaZ, h, sigma, nu)	

		integObj.integrate()

		"""
		addShedVorticity()
		loop()
		"""

if __name__=="__main__":
	runObj = VisVortex()
	runObj.runVisVort()
