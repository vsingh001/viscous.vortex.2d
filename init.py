import numpy as np
import scipy.sparse.linalg as spla
from util import Util

class Init:

	def __init__(self):
		self.constants()

	def constants(self):
		self.tolerance = 2.2e-16

		overlap = 1.3
		self.sigma = overlap

		self.nu = 0.10


	def init(self):
		"""
		Create a mesh of particles
		This will be the origin of the solution structure
		Once we determine a structure for the solution here it continues throughout
		Each vortex particle has location and cell total vorticity
		"""

		"""
		A rectangular domain with a square lattice setup
		"""
		a = -0.5
		b = 0.5	
		c = -0.5
		d = 0.5
		nc = 100

		tempX = np.linspace(a, b, nc)
		h = tempX[1] - tempX[0]
		self.sigma *= h

		tempY = np.linspace(c, d, nc)

		"""
		Particles occupy the center of cells spanned by tempX and tempY
		"""
		xc = 0.5*(tempX[1:] + tempX[:-1])
		yc = 0.5*(tempX[1:] + tempX[:-1])	

		[x,y] = np.meshgrid(xc, yc)

		circulation = 1
		t = 0.1
		vorticty = self.getLambOVortex(x,y, self.nu, circulation, t)
		vortFlat = np.ravel(vorticty)
		print 'Initial max vorticity is ' + str(max(vortFlat))

		"""
		Removing really small values. Based on Barba 04
		"""
		lowValueIndices = vortFlat*h*h < self.tolerance
		vortFlat[lowValueIndices] = 0	

		A = self.createAMatrix(x, y, h, self.sigma)

		alpha = spla.bicgstab(A, vortFlat, tol = 1e-15, maxiter = 80)
#		alpha = spla.lgmres(A, vortFlat, tol = 1e-08, maxiter = 50)

		lowValueIndices = np.abs(alpha[0]) < self.tolerance
		alpha[0][lowValueIndices] = 0

		alpha =  np.array(alpha[0])*h*h

#		self.write(x, y, vorticty, alpha)
		
#		self.genVort((99,99), np.ravel(x), np.ravel(y), alpha)

		return np.ravel(x), np.ravel(y), alpha, h, self.sigma, self.nu


	def write(self, x, y, vorticity, alpha):
		alpha = alpha.reshape(x.shape)
	
#		print alpha[0]

		wrFile = open('vort.dat', 'w')
#		wrFile1 = open('alpha.dat', 'w')

		for i in range(x.shape[0]):
			for j in range(x.shape[1]):	
				st = str(x[i,j])+'\t'+str(y[i,j])+'\t'+str(vorticity[i,j])+'\n'
				wrFile.write(st)
#				st = str(x[i,j])+'\t'+str(y[i,j])+'\t'+str(alpha[i,j])+'\n'	
#				wrFile1.write(st)
			wrFile.write('\n')
#			wrFile1.write('\n')

		wrFile.close()
#		wrFile1.close()


	def createAMatrix(self, x, y, h, sigma):
		xt = np.ravel(x)
		yt = np.ravel(y)

		ut = Util()

		noOfParticles = xt.shape[0]

		A = np.zeros((xt.shape[0], xt.shape[0]), float)	

		"""
		Vectorizing speeds up loop calculations a lot
		"""
		for i in range(noOfParticles):
			sqdistX = (xt[i] - xt)**2
			sqdistY = (yt[i] - yt)**2 
			dist = np.sqrt((sqdistX+sqdistY))/sigma
			A[i,:] = (h**2)*ut.xi(dist)/(sigma**2)

		lowValueIndices = A < self.tolerance
		A[lowValueIndices] = 0
	
		return A

	
	def getLambOVortex(self, x, y, nu, circulation, t):
		r = np.sqrt(x**2+y**2)
		vorticty = (circulation/(4*np.pi*nu*t))*np.exp(-1*(r**2)/(4*nu*t))

		return vorticty	


	def genVort(self, shape , x, y, alpha):
		ut = Util()

		vort = np.zeros(x.shape[0])
		for i in range(x.shape[0]):
			sqdistX = (x[i] - x)**2
			sqdistY = (y[i] - y)**2
			dist = np.sqrt((sqdistX+sqdistY))
			rho = dist/self.sigma		

			xiValues = ut.xi(rho)

			delVort = xiValues*alpha/(self.sigma**2)
			vort[i] = delVort.sum()
	
		lowValueIndices = np.abs(vort) < self.tolerance
		vort[lowValueIndices] = 0

		print max(vort)

		"""
		wrFile1 = open('vortCheck.dat', 'w')

		for i in range(shape[0]):
			for j in range(shape[1]):
				subscript = i*shape[1]+j
				st = str(x[subscript])+'\t'+str(y[subscript])+'\t'+str(vort[subscript])+'\n'
				wrFile1.write(st)
			wrFile1.write('\n')
	
		wrFile1.close()
		"""






if __name__=="__main__":
	runObj = Init()
	runObj.init()
