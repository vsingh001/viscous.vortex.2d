from util import Util
import numpy as np

class Integrate:



	def constants(self):
		self.tolerance = 2.2e-16
	
		"""
		kinematic viscosity
		"""
		self.nu = 1.3*(10**(-6))
		"""
		integrate flag
		1:Euler
		2:RK
		"""
		self.integFlag = 1
		"""
		Time step
		"""
		self.dt = 0.001
		"""
		Model constant
		"""
		return


	def pushInitValues(self, x, y, alphaZ, h, sigma, nu):
		"""
		The initialized values have to be passed once
		"""
		self.x = x
		self.y = y
		self.alphaZ = alphaZ
		self.h = h
		self.sigma = sigma 
		self.nu = nu
	
		return


	def integrate(self):
		"""
		First the two terms on the RHS have to be computed
		"""
		for i in range(5):
			print 'iteration'+'\t'+str(i+1)
			#We'll call them like this so that it is easily extended to FMM methods
			[delUX, delUY] = self.convect(self.x, self.y, self.alphaZ, self.sigma)
			
			#Simple Euler integration
			self.x = self.x + self.dt*delUX
			self.y = self.y + self.dt*delUY

#			self.writeVelocity((99, 99), self.x, self.y, delUX, delUY)

			diffuse = self.getDiffusion(self.x, self.y, self.alphaZ, self.h, self.sigma, self.nu)
			#Simple Euler integration
#			print max(self.alphaZ)
			[self.alphaZ] = [self.alphaZ] + self.dt*diffuse
#			print max(self.alphaZ)

			xm, ym, vorticity = self.genVort((99, 99), self.x, self.y, self.alphaZ)

		self.writeVort((99, 99), xm, ym, vorticity)



	def convect(self, x, y, alphaZ, sigma):
		noParticles = x.shape[0]
		ut = Util()

		cumUX = np.zeros_like(x)
		cumUY = np.zeros_like(x)
	
#		for i in [1]:
		for i in range(noParticles):
			sqdistX = (x[i] - x)**2
			sqdistY = (y[i] - y)**2
			dist = np.sqrt((sqdistX+sqdistY))
			rho = dist/sigma
			
			nonLowValueIndices = rho > self.tolerance
			kValues = np.zeros_like(x)
			kValues[nonLowValueIndices] = ut.K(rho[nonLowValueIndices])

			crossUX = -1*alphaZ*(y[i]-y)
			crossUY =  1*alphaZ*(x[i]-x)
			
			delUX = -1*kValues*crossUX/(sigma**2)
			delUY = -1*kValues*crossUY/(sigma**2)

			cumUX[i] += delUX.sum()
			cumUY[i] += delUY.sum()

		return [cumUX, cumUY]
	


	def getDiffusion(self, x, y, alphaZ, h, sigma, nu):
		noParticles = x.shape[0]
		ut = Util()

		cumLapl = np.zeros_like(x)

#		for i in [4901]:
		for i in range(noParticles):
			sqdistX = (x[i] - x)**2
			sqdistY = (y[i] - y)**2
			dist = np.sqrt((sqdistX+sqdistY))
			rho = dist/sigma
				
			nonLowValueIndices = rho > self.tolerance
			xiValues = np.zeros_like(x)
			xiValues[nonLowValueIndices] = ut.xi(rho[nonLowValueIndices])

			"""
			PSE as per He, Zhao. Something's not right
			"""
#			vI = h**2
#			vJ = h**2
#			laplacWt = vJ*alphaZ - vI*alphaZ[i]
			"""
			PSE as per Leonard, Koumoutsakos '93 and He, Zhao 
			"""
			laplacWt = (h**2/sigma**2)*(alphaZ - alphaZ[i])
			laplJ = laplacWt*xiValues

			cumLapl[i] = laplJ.sum()

		lowValueIndices = np.abs(cumLapl) < self.tolerance
		cumLapl[lowValueIndices] = 0

		cumLapl *= 2*nu/(sigma**2)

		return cumLapl


	def writeVelocity(self, shape, x, y, ux, uy):
		wrFile = open('veloc.dat', 'w')

		for i in range(shape[0]):
			for j in range(shape[1]):
				subscript = i*shape[1]+j
				speed = np.sqrt(ux[subscript]**2+uy[subscript]**2)
				st = str(x[subscript])+'\t'+str(y[subscript])+'\t'+str(speed)+'\n' 
				wrFile.write(st)				
			wrFile.write('\n')

		wrFile.close()


	def genVort(self, shape, x, y, alpha):
		ut = Util()
		"""
		A rectangular domain with a square lattice setup
		"""
		a = -0.5
		b = 0.5
		c = -0.5
		d = 0.5
		nc = 100

		tempX = np.linspace(a, b, nc)
		h = tempX[1] - tempX[0]

		tempY = np.linspace(c, d, nc)

		"""
		Particles occupy the center of cells spanned by tempX and tempY
		"""
		xc = 0.5*(tempX[1:] + tempX[:-1])
		yc = 0.5*(tempX[1:] + tempX[:-1])	

		[xm,ym] = np.meshgrid(xc, yc)
		xm = np.ravel(x)
		ym = np.ravel(y)

		vort = np.zeros(x.shape[0])
		for i in range(x.shape[0]):
			sqdistX = (xm[i] - x)**2
			sqdistY = (ym[i] - y)**2
			dist = np.sqrt((sqdistX+sqdistY))
			rho = dist/self.sigma		

			xiValues = ut.xi(rho)

			delVort = xiValues*alpha/(self.sigma**2)
			vort[i] = delVort.sum()
	
		lowValueIndices = np.abs(vort) < self.tolerance
		vort[lowValueIndices] = 0

		print 'Max vorticity after this iteration is ' + str(max(vort))

		return xm, ym, vort

		
	def writeVort(self, shape, x, y, vort):
		wrFile1 = open('vortT.dat', 'w')

		for i in range(shape[0]):
			for j in range(shape[1]):
				subscript = i*shape[1]+j
				st = str(x[subscript])+'\t'+str(y[subscript])+'\t'+str(vort[subscript])+'\n'
				wrFile1.write(st)
			wrFile1.write('\n')
	
		wrFile1.close()
		





